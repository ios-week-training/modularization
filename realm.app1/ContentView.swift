//
//  ContentView.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 07/03/24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            ContactView()
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
