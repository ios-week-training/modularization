//
//  ContactUseCase.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation
import Combine

class ListContactUseCase: UseCase {
    typealias Response = [ContactModel]
    typealias Request = ContactModel
    private let _repository: ListContactRepository<ContactLocaleDataSource, ListContactTransformer<ContactTransformer>>
    
    init(repository: ListContactRepository<ContactLocaleDataSource, ListContactTransformer<ContactTransformer>>) {
        self._repository = repository
    }
    
    func execute(req: Request?) -> AnyPublisher<Response, Error> {
        _repository.execute(req: nil)
    }
}
