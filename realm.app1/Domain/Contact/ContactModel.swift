//
//  ContactModel.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 08/03/24.
//

import Foundation

struct ContactModel: Identifiable {
    var id: UUID
    var firstName: String
    var lastName: String
    var phone: String
    var isBuddy: Bool = false
    
    var addresses = [AddressModel]()
    
    var fullName: String {
        return "\(firstName) \(lastName)"
    }

    init(contact: ContactEntity) {
        self.id = contact.id
        self.firstName = contact.firstName
        self.lastName = contact.lastName
        self.phone = contact.phone
        self.isBuddy = contact.isBuddy
        self.addresses = contact.addresses.map ( AddressMapper.mapEntityToDomain ).compactMap { $0 }
    }
    
    init(id: UUID = UUID(), firstName: String, lastName: String, phone: String, isBuddy: Bool = false) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.phone = phone
        self.isBuddy = isBuddy
    }
}
