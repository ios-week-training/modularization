//
//  ContactEntity.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 08/03/24.
//

import Foundation
import RealmSwift

class ContactEntity: Object, Identifiable {
    @Persisted(primaryKey: true) var id: UUID
    @Persisted var firstName: String
    @Persisted var lastName: String
    @Persisted var phone: String
    @Persisted var isBuddy: Bool = false
    
    @Persisted var addresses: List<AddressEntity>
}
