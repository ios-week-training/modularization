//
//  ToggleContactUseCase.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 13/03/24.
//

import Foundation
import Combine

class ToggleContactUseCase: UseCase {
    typealias Response = Bool
    typealias Request = UUID
    private let _repository: ToggleContactRepository<ContactLocaleDataSource>
    
    init(repository: ToggleContactRepository<ContactLocaleDataSource>) {
        self._repository = repository
    }
    
    func execute(req: Request?) -> AnyPublisher<Response, Error> {
        guard let request = req else {
            return Fail(error: DatabaseError.requestFailed).eraseToAnyPublisher()
        }
        return _repository.execute(req: request)
    }
}
