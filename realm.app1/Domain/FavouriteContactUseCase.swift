//
//  FavouriteContactUseCase.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 13/03/24.
//

import Foundation
import Combine

class FavouriteContactUseCase: UseCase {
    typealias Response = [ContactModel]
    typealias Request = ContactModel
    private let _repository: FavouriteContactRepository<ContactLocaleDataSource, ListContactTransformer<ContactTransformer>>
    
    init(repository: FavouriteContactRepository<ContactLocaleDataSource, ListContactTransformer<ContactTransformer>>) {
        self._repository = repository
    }
    
    func execute(req: Request?) -> AnyPublisher<Response, Error> {
        _repository.execute(req: nil)
    }
}
