//
//  SaveContactUseCase.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 13/03/24.
//
import Foundation
import Combine

class SaveContactUseCase: UseCase {
    typealias Response = Bool
    typealias Request = ContactModel
    private let _repository: SaveContactRepository<ContactLocaleDataSource, ContactTransformer>
    
    init(repository: SaveContactRepository<ContactLocaleDataSource, ContactTransformer>) {
        self._repository = repository
    }
    
    func execute(req: Request?) -> AnyPublisher<Response, Error> {
        _repository.execute(req: req)
    }
}
