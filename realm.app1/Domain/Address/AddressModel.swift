//
//  AddressModel.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 08/03/24.
//

import Foundation

struct AddressModel: Identifiable {
    var id: String
    var name: String
    var street: String
    var city: String
    var country: String
    
    init(address: AddressEntity) {
        self.id = address.id.stringValue
        self.name = address.name
        self.street = address.street
        self.city = address.city
        self.country = address.country
    }
    
    init(id: String, name: String, street: String, city: String, country: String) {
        self.id = id
        self.name = name
        self.street = street
        self.city = city
        self.country = country
    }
}
