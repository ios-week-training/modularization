//
//  AddressEntity.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 08/03/24.
//

import Foundation
import RealmSwift

class AddressEntity: Object, Identifiable {
    @Persisted(primaryKey: true) var id: ObjectId
    @Persisted var name: String
    @Persisted var street: String
    @Persisted var city: String
    @Persisted var country: String
}
