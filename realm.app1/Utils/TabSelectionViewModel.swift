//
//  tab.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 08/03/24.
//

import Foundation

class TabSelectionViewModel: ObservableObject {
    @Published var selectedTab: TabsNameEnum = .Contact
}
