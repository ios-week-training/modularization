//
//  NiceButtonStyle.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 08/03/24.
//

import Foundation
import SwiftUI

struct NiceButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(16)
            .background(Color.gray)
            .foregroundColor(.white)
            .cornerRadius(8)
            .font(.headline)
            .scaleEffect(configuration.isPressed ? 0.95 : 1.0)
            .animation(.easeInOut, value: 1.0)
    }
}
