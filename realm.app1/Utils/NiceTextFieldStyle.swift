//
//  NiceTextFieldStyle.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 08/03/24.
//
import Foundation
import SwiftUI

struct NiceTextFieldStyle: TextFieldStyle {
    func _body(configuration: TextField<_Label>) -> some View {
        configuration
            .padding(10)
            .background(Color.gray.opacity(0.1))
            .cornerRadius(8)
            .overlay(
                RoundedRectangle(cornerRadius: 8)
                    .stroke(Color.gray, lineWidth: 2)
            )
    }
}
