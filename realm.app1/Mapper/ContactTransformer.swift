//
//  ContactTransformer.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation

struct ContactTransformer: MapperProtocol {
    typealias Entity = ContactEntity
    typealias Domain = ContactModel
    
    func transformEntityToDomain(entity: Entity) -> Domain {
        return ContactModel(firstName: entity.firstName, lastName: entity.lastName, phone: entity.phone, isBuddy: entity.isBuddy)
    }
    
    func transformDomainToEntity(domain: ContactModel) -> ContactEntity {
        let contact = ContactEntity()
        contact.firstName = domain.firstName
        contact.lastName = domain.lastName
        contact.phone = domain.phone
        contact.isBuddy = domain.isBuddy
        return contact
    }
}
