//
//  GameDetailTransformer.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation

struct ListContactTransformer<Mapper: MapperProtocol>: MapperProtocol 
where
Mapper.Entity == ContactEntity,
Mapper.Domain == ContactModel
{
    typealias Entity = [ContactEntity]
    typealias Domain = [ContactModel]
    
    private var _contactListMapper: Mapper
    
    init(contactListMapper: Mapper) {
        _contactListMapper = contactListMapper
    }
    
    func transformEntityToDomain(entity: Entity) -> Domain {
        return entity.map { item in
            return _contactListMapper.transformEntityToDomain(entity: item)
        }
    }
    
    func transformDomainToEntity(domain: Domain) -> Entity {
        return domain.map { item in
            return _contactListMapper.transformDomainToEntity(domain: item)
        }
    }
}
