//
//  AddressMapper.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 08/03/24.
//

import Foundation

class AddressMapper {
    static func mapEntityToDomain( entity: AddressEntity) -> AddressModel {
        AddressModel(id: entity.id.stringValue, name: entity.name, street: entity.street, city: entity.city, country: entity.country)
    }
}
