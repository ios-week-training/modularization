//
//  DatabaseError.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation

public enum DatabaseError: LocalizedError {
    
    case invalidInstance
    case requestFailed
    
    public var errorDescription: String? {
        switch self {
        case .invalidInstance: return "Database can't instance."
        case .requestFailed: return "Your request failed."
        }
    }
}
