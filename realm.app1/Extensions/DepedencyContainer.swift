//
//  DepedencyInjector.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation
import RealmSwift
import SwiftUI

class DependencyContainer {    
    let realm: Realm
    let localeDataSource: ContactLocaleDataSource
    let contactTransformer: ContactTransformer
    let listContactTransformer: ListContactTransformer<ContactTransformer>
    let repository: ListContactRepository<ContactLocaleDataSource, ListContactTransformer<ContactTransformer>>
    let saveRepo: SaveContactRepository<ContactLocaleDataSource, ContactTransformer>
    let toggleRepo: ToggleContactRepository<ContactLocaleDataSource>
    let favouriteRepo: FavouriteContactRepository<ContactLocaleDataSource, ListContactTransformer<ContactTransformer>>
    
    init() {
        do {
            realm = try Realm()
        } catch {
            fatalError("Realm initialization failed: \(error)")
        }
        localeDataSource = ContactLocaleDataSource(realm: realm)
        contactTransformer = ContactTransformer()
        listContactTransformer = ListContactTransformer(contactListMapper: contactTransformer)
        repository = ListContactRepository(localeDataSource: localeDataSource, mapper: listContactTransformer)
        saveRepo = SaveContactRepository(localeDataSource: localeDataSource, mapper: contactTransformer)
        toggleRepo = ToggleContactRepository(localeDataSource: localeDataSource)
        favouriteRepo = FavouriteContactRepository(localeDataSource: localeDataSource, mapper: listContactTransformer)
    }
    
    func makeContactsPresenter() -> ContactPresenter {
        let getAllContactsUseCase = ListContactUseCase(repository: repository)
        let saveContactUseCase = SaveContactUseCase(repository: saveRepo)
        let toggleContactUseCase = ToggleContactUseCase(repository: toggleRepo)
        let favouriteContactUseCase = FavouriteContactUseCase(repository: favouriteRepo)
        return ContactPresenter(
            list: getAllContactsUseCase, save: saveContactUseCase, toggle: toggleContactUseCase, favourite: favouriteContactUseCase)
    }
}

