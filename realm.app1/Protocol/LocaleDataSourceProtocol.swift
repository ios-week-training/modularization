//
//  LocaleDataSourceProtocol.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation
import Combine

protocol LocaleDataSourceProtocol {
    associatedtype Response
    associatedtype Request
    
    func list() -> AnyPublisher<[Response], Error>
    func save(entity: Request) -> AnyPublisher<Bool, Error>
    func toggle(id: UUID) -> AnyPublisher<Bool, Error>
    func delete(id: String) -> AnyPublisher<Bool, Error>
}
