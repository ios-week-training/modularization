//
//  ContactRepository.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation
import Combine

protocol ContactRepositoryProtocol {
    associatedtype Response
    associatedtype Request
    
    func getAllContact() -> AnyPublisher<[Response], Error>
    func saveContact(contact: Request) -> AnyPublisher<Bool, Error>
    func removeContact(id: String) -> AnyPublisher<Bool, Error>
}
