//
//  RepositoryProtocol.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation
import Combine

public protocol RepositoryProtocol {
    associatedtype Request
    associatedtype Response
    
    func execute(req: Request?) -> AnyPublisher<Response, Error>
}
