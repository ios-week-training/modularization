//
//  UseCase.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation
import Combine

protocol UseCase {
    associatedtype Response
    associatedtype Request
    
    func execute(req: Request?) -> AnyPublisher<Response, Error>
//    func executes(req: Request?) -> AnyPublisher<[Response], Error>
}
