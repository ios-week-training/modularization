//
//  MapperProtocol.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation

public protocol MapperProtocol {
//    associatedtype Request
//    associatedtype Response
    associatedtype Entity
    associatedtype Domain
    
//    func transformResponseToEntity(request: Request?, response: Response) -> Entity
    func transformEntityToDomain(entity: Entity) -> Domain
    func transformDomainToEntity(domain: Domain) -> Entity
}
