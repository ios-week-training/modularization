//
//  ContactImpl.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation
import RealmSwift
import Combine

class SaveContactRepository<
    Locale: LocaleDataSourceProtocol,
    Transformer: MapperProtocol>: RepositoryProtocol
where
Locale.Request == ContactEntity,
Locale.Response == ContactEntity,
Transformer.Domain == ContactModel,
Transformer.Entity == ContactEntity
{
    public typealias Request = ContactModel
    public typealias Response = Bool
    private let _localeDataSource: Locale
    private let _mapper: Transformer
    
    public init(localeDataSource: Locale, mapper: Transformer) {
        _localeDataSource = localeDataSource
        _mapper = mapper
    }
    
    func execute(req: Request?) -> AnyPublisher<Response, Error> {
        guard let request = req else {
            return Fail(error: DatabaseError.requestFailed).eraseToAnyPublisher()
        }
        let entity = _mapper.transformDomainToEntity(domain: request)
        return _localeDataSource.save(entity: entity)
            .eraseToAnyPublisher()
    }
}

//    func saveContact(contact: ContactModel) -> AnyPublisher<Bool, Error> {
//        let req = ContactEntity()
//        req.firstName = contact.firstName
//        req.lastName = contact.lastName
//        req.phone = contact.phone
//        req.isBuddy = contact.isBuddy
//        return _localeDataSource.save(entity: req)
//            .eraseToAnyPublisher()
//    }
//
//    func removeContact(id: String) -> AnyPublisher<Bool, Error> {
//        return _localeDataSource.delete(id: id)
//            .eraseToAnyPublisher()
//    }
//}
