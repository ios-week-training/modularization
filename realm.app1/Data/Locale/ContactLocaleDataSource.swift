//
//  ContactLocaleDataSource.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation
import RealmSwift
import Combine

class ContactLocaleDataSource: LocaleDataSourceProtocol {
    typealias Response = ContactEntity
    typealias Request = ContactEntity
    
    private let _realm: Realm
    
    public init(realm: Realm) {
        _realm = realm
    }
    
    func list() -> AnyPublisher<[Response], Error> {
        return Future<[ContactEntity], Error> { completion in
            
            let contactEntities = {
                self._realm.objects(ContactEntity.self)
            }()
            completion(.success(Array(contactEntities)))
            
        }.eraseToAnyPublisher()
    }
    
    func save(entity: Request) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            do {
                try self._realm.write {
                    self._realm.add(entity)
                    completion(.success(true))
                }
            } catch {
                completion(.failure(DatabaseError.requestFailed))
            }
        }
        .eraseToAnyPublisher()
    }
    
    func toggle(id: UUID) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            do {
                guard let contactToUpdate = self._realm.object(ofType: ContactEntity.self, forPrimaryKey: id )
                else {
                    completion(.failure(DatabaseError.requestFailed))
                    return
                }
                
                print(contactToUpdate)
                print(contactToUpdate.isBuddy)
                try self._realm.write {
                    contactToUpdate.isBuddy = contactToUpdate.isBuddy == false ? true : false
                    completion(.success(true))
                }
                print("atas")
            } catch {
                completion(.failure(DatabaseError.requestFailed))
                print("bawah")
            }
        }
        .eraseToAnyPublisher()
    }
    
//    func update(id: UUID) -> AnyPublisher<Bool, Error> {
//        return Future<Bool, Error> { completion in
//            do {
//                let contact = self._realm.object(ofType: ContactEntity.self, forPrimaryKey: id)
//                try realm.write {
//                    contact?.firstName = firstName
//                    contact?.lastName = lastName
//                    contact?.phone = phone
//                    contact?.isBuddy = isBuddy
//                }
//            }
//        }
//        .eraseToAnyPublisher()
//    }
    
    func delete(id: String) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            do {
                let objectId = try ObjectId(string: id)
                if let contact = self._realm.object(ofType: ContactEntity.self, forPrimaryKey: objectId) {
                    try self._realm.write {
                        self._realm.delete(contact)
                    }
                }
            } catch {
                completion(.failure(DatabaseError.requestFailed))
            }
        }
        .eraseToAnyPublisher()
    }
}
