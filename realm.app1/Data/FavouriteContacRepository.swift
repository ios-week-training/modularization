//
//  FavouriteContacRepository.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 13/03/24.
//

import Foundation
import RealmSwift
import Combine

class FavouriteContactRepository<
    Locale: LocaleDataSourceProtocol,
    Transformer: MapperProtocol>: RepositoryProtocol
where
Locale.Request == ContactEntity,
Locale.Response == ContactEntity,
Transformer.Domain == [ContactModel],
Transformer.Entity == [ContactEntity]
{
    public typealias Request = ContactEntity
    public typealias Response = [ContactModel]
    private let _localeDataSource: Locale
    private let _mapper: Transformer
    
    public init(localeDataSource: Locale, mapper: Transformer) {
        _localeDataSource = localeDataSource
        _mapper = mapper
    }
    
    func execute(req: Request?) -> AnyPublisher<Response, Error> {
        return _localeDataSource.list()
            .map { self._mapper.transformEntityToDomain(entity: $0 ) }
            .map { model in
                model.filter { $0.isBuddy }
            }
            .eraseToAnyPublisher()
    }
}
