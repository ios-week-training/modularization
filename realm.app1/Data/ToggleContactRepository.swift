//
//  ToggleContactRepository.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 13/03/24.
//

import Foundation
import RealmSwift
import Combine

class ToggleContactRepository<Locale: LocaleDataSourceProtocol>: RepositoryProtocol
where
Locale.Request == ContactEntity,
Locale.Response == ContactEntity
{
    public typealias Request = UUID
    public typealias Response = Bool
    private let _localeDataSource: Locale
    
    public init(localeDataSource: Locale) {
        _localeDataSource = localeDataSource
    }
    
    func execute(req: Request?) -> AnyPublisher<Response, Error> {
        guard let request = req else {
            return Fail(error: DatabaseError.requestFailed).eraseToAnyPublisher()
        }
        return _localeDataSource.toggle(id: request)
            .eraseToAnyPublisher()
    }
}
