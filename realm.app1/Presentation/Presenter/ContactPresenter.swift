//
//  ContactPresenter.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import Foundation
import RealmSwift
import Combine

class ContactPresenter: ObservableObject {
    @Published var contacts = [ContactModel]()
    @Published var isLoading: Bool = false
    @Published var error: String?
    private var token: NotificationToken?
    
    private var cancellables = Set<AnyCancellable>()
    private let listUseCase: ListContactUseCase
    private let saveUseCase: SaveContactUseCase
    private let toggleUseCase: ToggleContactUseCase
    private let favouriteUseCase: FavouriteContactUseCase
    
    init(list: ListContactUseCase, save: SaveContactUseCase, toggle: ToggleContactUseCase, favourite: FavouriteContactUseCase) {
        listUseCase = list
        saveUseCase = save
        toggleUseCase = toggle
        favouriteUseCase = favourite
    }

    deinit {
        token?.invalidate()
    }
    
    func loadContacts() {
        isLoading = true
        error = nil

        listUseCase.execute(req: nil)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                self?.isLoading = false
                switch completion {
                case .failure(let error):
                    self?.error = error.localizedDescription
                case .finished:
                    break
                }
            }, receiveValue: { contactList in
                self.contacts = contactList
            })
            .store(in: &cancellables)
    }
    
    func loadFavouriteContacts() {
        isLoading = true
        error = nil

        favouriteUseCase.execute(req: nil)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                self?.isLoading = false
                switch completion {
                case .failure(let error):
                    self?.error = error.localizedDescription
                case .finished:
                    break
                }
            }, receiveValue: { contactList in
                self.contacts = contactList
            })
            .store(in: &cancellables)
    }
    
    func saveContact(contact: ContactModel) {
        saveUseCase.execute(req: contact)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                self?.isLoading = false
                switch completion {
                case .failure(let error):
                    self?.error = error.localizedDescription
                case .finished:
                    break
                }
            }, receiveValue: { contacts in
                if contacts == true {
                    self.loadContacts()
                }
            })
            .store(in: &cancellables)
    }
    
    func toggleBuddy(id: UUID) {
        toggleUseCase.execute(req: id)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                self?.isLoading = false
                switch completion {
                case .failure(let error):
                    self?.error = error.localizedDescription
                case .finished:
                    break
                }
            }, receiveValue: { contacts in
                if contacts == true {
                    self.loadContacts()
                    print("lewat")
                }
            })
            .store(in: &cancellables)
    }
//
//
//    init(getAllContactsUseCase: GetAllContactsUseCase) {
//        self.getAllContactsUseCase = getAllContactsUseCase
//    }
//
//    func loadContacts() {
//        isLoading = true
//        getAllContactsUseCase.execute()
//            .sink(receiveCompletion: { completion in
//                self.isLoading = false
//                if case let .failure(error) = completion {
//                    self.error = error
//                }
//            }, receiveValue: { contacts in
//                self.contacts = contacts
//            })
//            .store(in: &cancellables)
//    }
}
