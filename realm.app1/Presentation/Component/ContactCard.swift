//
//  ContactCard.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 08/03/24.
//

import SwiftUI

struct ContactCard: View {
    @State private var isBuddy: Bool = false
    var toogleBuddy: (UUID) -> Void
    var contact: ContactModel
    
    var body: some View {
        VStack(spacing: 8) {
            HStack {
                VStack(spacing: 8){
                    HStack {
                        Text("Name:")
                        Text(contact.fullName)
                        Spacer()
                    }
                    HStack {
                        Text("Phone:")
                        Text(contact.phone)
                        Spacer()
                    }}
                Button(action: {
                    isBuddy.toggle()
                    toogleBuddy(contact.id)
                }) {
                    Image(systemName: contact.isBuddy ? "heart.fill": "heart")
                        .resizable()
                        .frame(width: 24, height: 24)
                        .foregroundColor( contact.isBuddy ? Color.red: Color.gray)
                        .padding(.trailing)
                }
            }
        }
        .onAppear{
            isBuddy = contact.isBuddy
        }
        .padding(.all, 16)
    }
}

//#Preview {
//    ContactCard(toogleBuddy: ContactPresenter.toggleBuddy(id:) as! (UUID) -> Void, contact: ContactModel(id: UUID(), firstName: "Iqbal", lastName: "Rahman", phone: "082447462374"))
//}
