//
//  ContactList.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import SwiftUI
import RealmSwift

struct ContactList: View {
    @ObservedObject var contactPresenter: ContactPresenter
    
    var body: some View {
        VStack {
            ScrollView {
                LazyVStack (alignment: .leading) {
                    ForEach(contactPresenter.contacts) { contact in
                        ContactCard(
                            toogleBuddy: contactPresenter.toggleBuddy(id:),
                            contact: contact
                        )
//                            .onTapGesture {
//                                isDetailRoute.toggle()
//                            }
//                            .navigationDestination(isPresented: $isDetailRoute) {
//                                ContactDetail(contact: contact, viewModel: viewModel)
//                            }
                        Divider().padding(.leading, 20)
                    }
                }
            }
        }
        .onAppear {
            contactPresenter.loadContacts()
        }
    }
}

#Preview {
    ContactList(
        contactPresenter: ContactPresenter(
            list: ListContactUseCase(
                repository: ListContactRepository(
                    localeDataSource: ContactLocaleDataSource(
                        realm: try! Realm()),
                    mapper: ListContactTransformer(
                        contactListMapper: ContactTransformer()))),
            save: SaveContactUseCase(
                repository: SaveContactRepository(
                    localeDataSource: ContactLocaleDataSource(
                        realm: try! Realm()),
                    mapper: ContactTransformer())),
            toggle: ToggleContactUseCase(
                repository: ToggleContactRepository(
                    localeDataSource: ContactLocaleDataSource(
                        realm: try! Realm()))),
            favourite: FavouriteContactUseCase(
                repository: FavouriteContactRepository(
                    localeDataSource: ContactLocaleDataSource(
                        realm: try! Realm()),
                    mapper: ListContactTransformer(
                        contactListMapper: ContactTransformer())))
        )
    )
}
