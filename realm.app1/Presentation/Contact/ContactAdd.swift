//
//  ContactAdd.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import SwiftUI
import RealmSwift

struct ContactAdd: View {
    var contactPresenter: ContactPresenter
    
    @State private var firstName: String = ""
    @State private var lastName: String = ""
    @State private var phone: String = ""
    @State private var isBuddy: Bool = false
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        VStack(spacing: 20) {
            Text("Enter contact details")
            TextField("Enter First Name", text: $firstName)
                .textFieldStyle(NiceTextFieldStyle())
            TextField("Enter Last Name", text: $lastName)
                .textFieldStyle(NiceTextFieldStyle())
            TextField("Enter Phone Number", text: $phone)
                .textFieldStyle(NiceTextFieldStyle())
            Toggle("My Buddy?", isOn: $isBuddy)
            Button(action: submitContact) {
                Text("Add Contact")
                    .foregroundColor(.orange)
            }
            .buttonStyle(NiceButtonStyle())
        }
        .padding(20)
    }
    
    private func submitContact() {
        contactPresenter.saveContact(
            contact: ContactModel(firstName: firstName, lastName: lastName, phone: phone, isBuddy: isBuddy)
        )
        dismiss()
    }
}

#Preview {
    ContactAdd(
        contactPresenter: ContactPresenter(
            list: ListContactUseCase(
                repository: ListContactRepository(
                    localeDataSource: ContactLocaleDataSource(
                        realm: try! Realm()),
                    mapper: ListContactTransformer(
                        contactListMapper: ContactTransformer()))),
            save: SaveContactUseCase(
                repository: SaveContactRepository(
                    localeDataSource: ContactLocaleDataSource(
                        realm: try! Realm()),
                    mapper: ContactTransformer())),
            toggle: ToggleContactUseCase(
                repository: ToggleContactRepository(
                    localeDataSource: ContactLocaleDataSource(
                        realm: try! Realm()))),
            favourite: FavouriteContactUseCase(
                repository: FavouriteContactRepository(
                    localeDataSource: ContactLocaleDataSource(
                        realm: try! Realm()),
                    mapper: ListContactTransformer(
                        contactListMapper: ContactTransformer())))
        )
    )
}
