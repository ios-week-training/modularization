//
//  ContactView.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 12/03/24.
//

import SwiftUI

enum TabsNameEnum: String, Hashable {
    case Contact
    case Favourite
}

struct ContactView: View {
    @EnvironmentObject var contactPresenter: ContactPresenter
    @StateObject private var viewModel = TabSelectionViewModel()
    
    @State var addContact: Bool = false
    
    var body: some View {
        NavigationStack {
            TabView(selection: $viewModel.selectedTab) {
                Group {
                    ContactList(contactPresenter: contactPresenter)
                        .tabItem {
                            Label("Contact", systemImage: "book.closed.fill")
                        }
                        .tag(TabsNameEnum.Contact)
                    
                    FavouriteList(contactPresenter: contactPresenter)
                        .tabItem {
                            Label("Favourite", systemImage: "star.fill")
                        }
                        .tag(TabsNameEnum.Favourite)
                }
            }
            .navigationTitle(viewModel.selectedTab.rawValue)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {
                        addContact.toggle()
                    }) {
                        Label("Add", systemImage: "person.badge.plus")
                    }
                }
            }
            .sheet(isPresented: $addContact) {
                ContactAdd(contactPresenter: contactPresenter)
            }
            .padding()
        }
    }
}

#Preview {
    ContactView()
}
