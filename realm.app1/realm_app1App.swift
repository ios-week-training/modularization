//
//  realm_app1App.swift
//  realm.app1
//
//  Created by Iqbal Rahman on 07/03/24.
//

import SwiftUI

@main
struct realm_app1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(DependencyContainer().makeContactsPresenter())
        }
    }
}
